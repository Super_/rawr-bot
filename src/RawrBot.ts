import { DBConnection } from "./connection/DBConnection";
import { Client, Message, Guild, GuildMember } from "discord.js";
import { resolve } from "path";

import { PlaylistDatabase } from "./music/connection/PlaylistDatabase";
import { GuildDatabase } from "./connection/GuildDatabase";
import { CommandProcessor } from "./commands/CommandProcessor";
import { MusicManager } from "./music/services/MusicManager";
import { Utils } from "./utils/Utils";

const botConfig = require('../config.json');
export class RawrBot {
  public static readonly VERSION:string = '0.4.1';

  private _bot: Client;
  private _databaseConnection: DBConnection;
  private _playlistDatabase: PlaylistDatabase;
  private _guildDatabase: GuildDatabase;
  private _commandProcessor: CommandProcessor;
  private _musicManager: MusicManager;

  constructor(token: string, db_user:string, db_pass:string, db_name:string) {
    console.log('Starting ' + botConfig.bot_name);

    this._bot = new Client();
    this._bot.on('ready', () => this.onReady());

    this.setup(token, db_user, db_pass, db_name);
  }

  private async setup(token: string, db_user:string, db_pass:string, db_name:string): Promise<void> {
    try {
      console.log('Connecting to database ... ');
      this._databaseConnection = new DBConnection(db_user, db_pass, db_name);
      await this._databaseConnection.connect();
      console.log('success. \n');

      this._guildDatabase = new GuildDatabase(this._databaseConnection.connection);
      this._playlistDatabase = new PlaylistDatabase(this._databaseConnection.connection);
      this._musicManager = new MusicManager(this._playlistDatabase);
      this._commandProcessor = new CommandProcessor(botConfig.prefix, this._musicManager);

      console.log('Connecting to discord ... ');
      await this._bot.login(token);
      console.log('success. \n \n');

      this._bot.on('message', (message) => this.onMessage(message));
      this._bot.on('guildCreate', (guild) => this.onGuildCreate(guild));
      this._bot.on('guildDelete', (guild) => this.onGuildDelete(guild));
      this._bot.on('voiceStateUpdate', (oldMember, newMember) => this.onVoiceStateUpdate(oldMember, newMember));

      console.log('I am now online!');
    } catch (err) {
      Utils.logError('RawrBot', 'setup', err);
    }
  }

  private onReady(): void {
    this._bot.user.setActivity(`In BETA on ${this._bot.guilds.size} servers`)
  }

  private onMessage(message: Message): void {
    this._commandProcessor.process(message, this._bot);
  }

  private onGuildCreate(guild: Guild): void {
    this._guildDatabase.addGuild(guild.id);

    this.onReady();
  }

  private onGuildDelete(guild: Guild): void {
    this._guildDatabase.deleteGuild(guild.id);

    this.onReady();
  }

  private onVoiceStateUpdate(oldMember: GuildMember, newMember: GuildMember): void {
    if (newMember.user.id === this._bot.user.id  && newMember.voiceChannel !== undefined) {
      this._musicManager.checkShouldLeaveVoice(newMember.voiceChannel.guild.id, newMember, this._bot);
    } else if ((oldMember.voiceChannel !== undefined && newMember.voiceChannel === undefined)) {
      this._musicManager.checkShouldLeaveVoice(oldMember.voiceChannel.guild.id, oldMember, this._bot);
    }
  }
}
const x = new RawrBot(process.argv[2], process.argv[3], process.argv[4], process.argv[5]);
