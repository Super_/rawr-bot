import { Snowflake, VoiceConnection, VoiceChannel, TextChannel } from "discord.js";
import { PlaylistDatabase } from "./PlaylistDatabase";
import { MusicConnection } from "./MusicConnection";

/**
 * Manages connections for guilds.
 * 
 * @class MusicConnectionManager
 */
export class MusicConnectionManager {
  private readonly _playlistDatabase: PlaylistDatabase;
  private readonly _connectionsMap: Map<Snowflake, MusicConnection>;

  constructor(playlistDatabase: PlaylistDatabase) {
    this._playlistDatabase = playlistDatabase;
    this._connectionsMap = new Map<Snowflake, MusicConnection>();
  }

  add(guildId: Snowflake, connectionObj: { voiceConnection: VoiceConnection, voiceChannel: VoiceChannel, textChannel: TextChannel, streamDispatcherVolume: number }): MusicConnection {
    const currentConnection = new MusicConnection(
      guildId,
      connectionObj.voiceConnection,
      connectionObj.voiceChannel,
      connectionObj.textChannel,
      this._playlistDatabase,
      connectionObj.streamDispatcherVolume
    );

    this._connectionsMap.set(guildId, currentConnection);
    return currentConnection;
  }

  destroy(guildId: Snowflake): void {
    this._connectionsMap.delete(guildId);
  }

  get(guildId: Snowflake): MusicConnection {
    if(!this._connectionsMap.has(guildId)) {
      return undefined;
    } else {
      return this._connectionsMap.get(guildId);
    }
  }
}