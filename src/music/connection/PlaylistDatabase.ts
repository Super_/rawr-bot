import { Client } from "pg";
import { Snowflake } from "discord.js";
import { IPlaylist } from "../interfaces/IPlaylist";
import { IRequestedSong } from "../interfaces/IRequestedSong";
import { IDBSongResult } from "../interfaces/IDBSongResult";

export class PlaylistDatabase {
  private _connection: Client;

  constructor(connection: Client) {
    this._connection = connection;
  }

  addToPlaylist(guildId: Snowflake, playlistName: string, song: IRequestedSong): Promise<void> {
    return new Promise(async (resolve, reject) => {
      let playlistId = await this.getPlaylistID(guildId, playlistName);
      if (playlistId === undefined) {
        playlistId = await this.createPlaylist(
          guildId,
          playlistName,
          'system'
        );
      }

      this._connection.query(
        'INSERT INTO song (playlist_id, title, url, requested_by) VALUES ($1, $2, $3, $4)', [playlistId, song.title, song.url, song.requestedby],
        (error, results) => {
          if (error) reject(error);

          resolve();
        }
      );
    });
  }

  getNextSong(guildId: Snowflake, playlistName: string): Promise<IDBSongResult | undefined> {
    return new Promise((resolve, reject) => {
      this._connection.query(
        'SELECT id, title, URL, requested_by AS requestedby FROM song WHERE playlist_id = (SELECT id FROM playlist WHERE guild_id = $1 AND name = $2) ORDER BY id ASC LIMIT 1', [guildId, playlistName],
        (error, result) => {
          if (error) reject(error);

          resolve(result.rowCount === 1 ? result.rows[0] : undefined);
        }
      );
    });
  }

  removeFromPlaylist(songIds: number[]): Promise<void> {
    return new Promise((resolve, reject) => {
      this._connection.query(
        'DELETE FROM song WHERE id = ANY ($1)', [songIds],
        (error, results) => {
          if (error) reject(error);

          resolve();
        }
      );
    });
  }

  clearPlaylist(guildId: Snowflake, playlistName: string): Promise<void> {
    return new Promise((resolve, reject) => {
      this._connection.query(
        'DELETE FROM song WHERE playlist_id = (SELECT id FROM playlist WHERE guild_id = $1 AND name = $2)', [guildId, playlistName],
        (error, results) => {
          if (error) reject(error);

          resolve();
        }
      );
    });
  }

  getPlaylistID(guildId: Snowflake, playlistName: string): Promise<number | undefined> {
    return new Promise((resolve, reject) => {
      this._connection.query(
        'SELECT id FROM playlist WHERE guild_id = $1 AND name = $2', [guildId, playlistName],
        (error, result) => {
          if (error) reject(error);

          resolve(result.rowCount === 1 ? result.rows[0].id : undefined);
        }
      );
    });
  }

  createPlaylist(guildId: Snowflake, playlistName: string, createdBy: string): Promise<number> {
    return new Promise((resolve, reject) => {
      this._connection.query(
        'INSERT INTO playlist (guild_id, name, created_by) VALUES ($1, $2, $3) RETURNING id', [guildId, playlistName, (createdBy === undefined ? 'system' : createdBy)],
        (error, result) => {
          if (error) reject(error);

          resolve(result.rows[0].id);
        }
      );
    });
  }


  deletePlaylist(guildId: Snowflake, playlistName: string): Promise<void> {
    return new Promise((resolve, reject) => {
      this._connection.query(
        'DELETE FROM playlist WHERE guild_id = $1 AND name = $2', [guildId, playlistName],
        (error, result) => {
          if (error) reject(error);

          resolve();
        }
      );
    });
  }

  getPlaylist(guildId: Snowflake, playlistName: string): Promise<IPlaylist[]> {
    return new Promise((resolve, reject) => {
      this._connection.query(
        'SELECT row_number() OVER () AS resultIndex, s.id, s.title, s.url FROM song s WHERE playlist_id = (SELECT id FROM playlist WHERE guild_id = $1 AND name = $2) ORDER BY id ASC', [guildId, playlistName],
        (error, result) => {
          if (error) reject(error);

          resolve(result.rows);
        }
      );
    });
  }

  getPlaylists(guildId: Snowflake): Promise<object[]> {
    return new Promise((resolve, reject) => {
      this._connection.query(
        'SELECT name FROM playlist WHERE guild_id = $1', [guildId],
        (error, result) => {
          if (error) reject(error);

          resolve(result.rows);
        }
      );
    });
  }
}