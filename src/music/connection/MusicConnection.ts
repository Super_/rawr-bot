import { Snowflake, VoiceConnection, VoiceChannel, TextChannel, StreamDispatcher } from "discord.js";

import { PlaylistDatabase } from "./PlaylistDatabase";
import { Utils } from "../../utils/Utils";
import { IDBSongResult } from "../interfaces/IDBSongResult";
import { IRequestedSong } from "../interfaces/IRequestedSong";
import { MusicManager } from "../services/MusicManager";
import { NowPlayingEmbed } from "../../commands/commands/embeds/NowPlayingEmbed";

/**
 * Stores the current connection for a guild.
 * 
 * @class MusicConnection
 */
export class MusicConnection {
  private readonly _guildId: Snowflake;
  private readonly _voiceConnection: VoiceConnection;
  private readonly _voiceChannel: VoiceChannel;
  private readonly _textChannel: TextChannel;
  private readonly _playlistDatbase: PlaylistDatabase;
  private readonly _streamDispatcherVolume: number;
  private _streamDispatcher: StreamDispatcher;
  private _currentSong: IDBSongResult;

  constructor(guildId: Snowflake, voiceConnection: VoiceConnection, voiceChannel: VoiceChannel, textChannel: TextChannel, playlistDatabase: PlaylistDatabase, streamDispatcherVolume: number) {
    this._guildId = guildId;
    this._voiceConnection = voiceConnection;
    this._voiceChannel = voiceChannel;
    this._textChannel = textChannel;
    this._playlistDatbase = playlistDatabase;
    this._streamDispatcherVolume = streamDispatcherVolume;
  }

  /**
   * Starts playing from the playlist for this guild.
   * 
   * @param {Function} onPlaylistEmpty called when the playlist is empty.
   * @returns {void}
   * @memberof MusicConnection
   */
  async start(musicManager: MusicManager, onPlaylistEmpty: (guildId: Snowflake, textChannel: TextChannel, message: string) => void): Promise<void> {
    try {
      const song = await this._playlistDatbase.getNextSong(this._guildId, 'default');

      if (song === undefined) {
        this.stop();
        onPlaylistEmpty(this._guildId, this._textChannel, 'Leaving because the playlist is empty! Goodbye!');
      } else {
        this._currentSong = song;

        const streamableURL = await musicManager.getServiceFromURL(song.url).getReadableStream(song);
        if (streamableURL === null) {
          // TODO: something.
        } else {
          this._streamDispatcher = this._voiceConnection.playArbitraryInput(streamableURL,
            { passes: 2 }
          );

          this._streamDispatcher.setVolume(this._streamDispatcherVolume);

          this._streamDispatcher.on('start', async () => {
            this._playlistDatbase.removeFromPlaylist([song.id]);
            const playlistEmbed = await new NowPlayingEmbed(song).getEmbed();
            this._textChannel.send(playlistEmbed);
          });

          this._streamDispatcher.on('end', async (reason) => {
            if (reason !== 'stop') {
              this.start(musicManager, onPlaylistEmpty);
            }
          });

          this._streamDispatcher.on('error', err => Utils.logError('MusicConnection', 'start', err.message));
        }
      }
    } catch (err) {
      Utils.logError('MusicConnection', 'start', err);
    }
  }

  /**
   * Stops the stream.
   */
  stop(): void {
    if (this._streamDispatcher !== null) {
      this._streamDispatcher.end('stop');
    }
  }

  skip(): void {
    if (this._streamDispatcher !== null) {
      this._streamDispatcher.end('skip');
    }
  }

  /**
   * Pauses the stream.
   * 
   * TODO: Make it an embed at say what song is paused.
   */
  pause(): void {
    this._textChannel.send('RawrBot paused at ' + Utils.millisecondsToMinutesAndSeconds(this._streamDispatcher.time));
    this._streamDispatcher.pause();
  }

  /**
   * Resumes the stream.
   * 
   * TODO: Make it an embed at say what song is resumed.
   */
  resume(): void {
    this._textChannel.send('RawrBot resumed at ' + Utils.millisecondsToMinutesAndSeconds(this._streamDispatcher.time));
    this._streamDispatcher.resume();
  }

  setVolume(newVolume: number): void {
    this._streamDispatcher.setVolume(newVolume);
  }

  get guildId(): Snowflake {
    return this._guildId;
  }

  get voiceConnection(): VoiceConnection {
    return this._voiceConnection;
  }

  get voiceChannel(): VoiceChannel {
    return this._voiceChannel;
  }

  get textChannel(): TextChannel {
    return this._textChannel;
  }

  get streamDispatcher(): StreamDispatcher {
    return this._streamDispatcher;
  }

  get currentSong(): IRequestedSong {
    return this._currentSong;
  }

  get currentVolume(): number {
    return this._streamDispatcher.volume;
  }
}