import { Utils } from "../../utils/Utils";
import { escape } from 'querystring';
import * as ch from 'cheerio';
import { request } from 'https';
import { ISong } from "../interfaces/ISong";
import { IOrderableSong } from "../interfaces/IOrderableSongList";
import { IMusicService } from "../interfaces/IMusicService";

import { Readable } from "stream";
import * as ytdlcore from "ytdl-core";

/**
 * Contains all things related to YouTube specific streaming.
 * 
 * @class YouTube
 * @extends {MusicService}
 */
export class YouTube implements IMusicService {
  private readonly _maxSearchResults: number;

  constructor(maxSearchResults: number) {
    this._maxSearchResults = maxSearchResults;
  }

  /**
   * Searches YouTube for the given message.
   * 
   * @param {String} message the message to search for.
   * @returns {Promise} haven't decided what to return yet.
   * @memberof YouTube
   */
  search(message: string): Promise<{ searchString: string, results: IOrderableSong[] }> {
    return new Promise((resolve) => {
      const req = request({
        hostname: 'www.youtube.com',
        port: 443,
        path: '/results?search_query=' + escape(message),
        method: 'GET'
      }, (res) => {
        let readData = '';
        res.on('data', (d) => readData += d);

        res.on('end', () => {
          const $ = ch.load(readData);
          const searchResults = $('.yt-lockup-video').slice(0, 10);
          const resultArray: IOrderableSong[] = [];

          searchResults.each((i, videoDIV) => {
            const videoLink = $(videoDIV).find('.yt-lockup-title > a');

            ytdlcore.getInfo(videoLink.attr('href'), async (error, info) => {
              if (error) {
                return;
              } 

              // cast to any to stop moaning about live_playback not existing.
              if (!(info as any).live_playback) {
                resultArray.push({
                  resultIndex: i,
                  title: info.title,
                  url: info.video_url
                });
              }

              if (resultArray.length === this._maxSearchResults) {
                const filteredResults = [...resultArray.sort((a, b) => a.resultIndex - b.resultIndex).slice(0, this._maxSearchResults)];

                resolve({ searchString: message, results: filteredResults });
              }
            });
          });
        });
      });

      req.on('error', (err) => Utils.logError('YouTube', 'search', err.message));
      req.end();
    });
  }

  getInformationFromURL(url: string): Promise<ISong> {
    return new Promise((resolve, reject) => {
      ytdlcore.getInfo(url, (error, info) => {
        if (error) reject(null);

        resolve({
          title: info.title,
          url: info.video_url
        });
      });
    });
  }

  validateURL(url: string): boolean {
    // cast to any to ignore TSC moaning validateURL and validateID don't exist (they do).
    return (ytdlcore as any).validateURL(url) || (ytdlcore as any).validateID(url);
  }

  getReadableStream(song: ISong): Promise<string> {
    return new Promise((resolve, reject) => {
      ytdlcore.getInfo(song.url, (error, info) => {
        const audioFormats = ytdlcore.filterFormats(info.formats, 'audioonly').sort((f1, f2) => f2.audioBitrate - f1.audioBitrate);

        if (audioFormats.length > 0) {
          resolve(audioFormats[0].url);
        } else {
          reject(null);
        }
      });
    });
  }
}