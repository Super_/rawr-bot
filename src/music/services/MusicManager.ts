import { MusicConnectionManager } from "../connection/MusicConnectionManager";
import { BotController } from "../controllers/BotController";
import { PlaylistDatabase } from "../connection/PlaylistDatabase";
import { Snowflake, VoiceChannel, TextChannel, VoiceConnection, Client, GuildMember } from "discord.js";
import { MusicConnection } from "../connection/MusicConnection";
import { PlaylistController } from "../controllers/PlaylistController";
import { MusicInstances } from "../instances/MusicInstances";
import { YouTube } from "../services/YouTube";
import { clearTimeout } from "timers";
import { IMusicService } from "../interfaces/IMusicService";
import { IConnection } from "../interfaces/IConnection";
import { MessageEmbed } from "../../commands/commands/embeds/MessageEmbed";
import { EmbedType } from "../../commands/commands/embeds/EmbedType";

export class MusicManager {
  private readonly _musicConnectionManager: MusicConnectionManager;
  private readonly _playlistController: PlaylistController;
  private readonly _botController: BotController;
  private readonly _instances: MusicInstances;
  private _mutedTimer : NodeJS.Timer;

  constructor(playlistDatabase: PlaylistDatabase) {
    this._musicConnectionManager = new MusicConnectionManager(playlistDatabase);
    this._playlistController = new PlaylistController(this, playlistDatabase);
    this._botController = new BotController(this);
    this._mutedTimer = null;

    this._instances = new MusicInstances({});
  }

  getService(serviceName: string): IMusicService {
    return this._instances.get(serviceName);
  }

  getServiceFromURL(url: string): IMusicService {
    return Array.from(this._instances.instanceMap).map(item => item[1]).filter(service => service.validateURL(url))[0];        
  }

  getCurrentConnection(guildId: Snowflake): MusicConnection {
    return this._musicConnectionManager.get(guildId);
  }

  isPlaying(guildId: Snowflake): MusicConnection | boolean {
    return this.getCurrentConnection(guildId);
  }

  getBotVoiceChannel(guildId: Snowflake): VoiceChannel {
    const isPlaying = this.isPlaying(guildId);
    if (!isPlaying) {
      return undefined;
    } else {
      return (<MusicConnection>isPlaying).voiceChannel;
    }
  }

  checkShouldLeaveVoice(guildId: Snowflake, member: GuildMember, bot: Client): void {
    const isPlaying = this.isPlaying(guildId);

    if (isPlaying) {
      const musicConnection = <MusicConnection>isPlaying;

      if (member.user.id === bot.user.id) {
        if(member.serverMute) {
          if(this._mutedTimer === null) {
            musicConnection.textChannel.send(
              new MessageEmbed(
                EmbedType.INFORMATION,
                'I was muted! If I am still muted in 15 seconds I am leaving! :rage:'
              )
            )
            this._mutedTimer = setTimeout(() => {
              this._botController.stopPlaying(
                guildId, 
                musicConnection.textChannel, 
                false, 
                'I was muted for 15 seconds so I left :cry:');
            }, 15000);
          }
        } else {
          if(this._mutedTimer !== null) {
            clearTimeout(this._mutedTimer);
          }
        }
      } else {
        const voiceChannelMembers = musicConnection.voiceChannel.members;
        const allBots = voiceChannelMembers.array().every(value => value.user.bot);

        if (voiceChannelMembers.size === 0 || allBots) {
          this._botController.stopPlaying(
            guildId, 
            musicConnection.textChannel, 
            false, 
            'I have monophobia and everyone left, so I left to find new friends. :persevere:')
        }
      }
    }
  }

  addConnection(guildId: Snowflake, connectionObj: IConnection): MusicConnection {
    return this._musicConnectionManager.add(guildId, connectionObj);
  }

  destroyConnection(guildId: Snowflake): void {
    this._musicConnectionManager.destroy(guildId);
  }

  get botController(): BotController {
    return this._botController;
  }

  get playlistController(): PlaylistController {
    return this._playlistController;
  }
}