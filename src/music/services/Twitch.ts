import { IMusicService } from "../interfaces/IMusicService";
import { ISong } from "../interfaces/ISong";
import { IOrderableSong } from "../interfaces/IOrderableSongList";

import { exec } from "child_process";
import { Readable } from "stream";
import * as ch from 'cheerio';
import { request } from 'https';
import { Utils } from "../../utils/Utils";
import { escape } from 'querystring';

export class Twitch implements IMusicService {

  search(message: string): Promise<{ searchString: string; results: IOrderableSong[]; }> {
    throw new Error("Method not implemented.");
  }
  
  // TODO: Replace all NULL rejects with error messages.
  // TODO: SORT THE SHIT OUT OF THIS
  getInformationFromURL(url: string): Promise<ISong> {
    console.log(escape(url.substring(url.indexOf('.tv/') + 4)));
    return new Promise((resolve, reject) => {
      const req = request({
        hostname: 'api.twitch.tv',
        port: 443,
        path: '/kraken/users?login=' + escape(url.substring(url.indexOf('.tv/') + 4)),
        method: 'GET',
        headers: {
          'Accept': 'application/vnd.twitchtv.v5+json',
          'Client-ID': 'c9fm5lusvrzyyir6og937om0u0pqd5',
          'Authorization': 'OAuth qystlet3r83krolqns4rzvq72in8gr',
        }
      }, (res) => {
        let readData = '';
        res.on('data', (d) => readData += d);

        res.on('end', async () => {
          if (readData.length === 0) {
            reject(null);
          } else {
            const parsedJSON = JSON.parse(readData);
            if (parsedJSON.users.length !== 1) {
              reject(null);
            } else {
              const userId = parsedJSON.users[0]._id;
              const req = request({
                hostname: 'api.twitch.tv',
                port: 443,
                path: '/kraken/streams/' + escape(userId),
                method: 'GET',
                headers: {
                  'Accept': 'application/vnd.twitchtv.v5+json',
                  'Client-ID': 'c9fm5lusvrzyyir6og937om0u0pqd5',
                  'Authorization': 'OAuth qystlet3r83krolqns4rzvq72in8gr',
                }
              }, (res) => {
                let readData = '';
                res.on('data', (d) => readData += d);

                res.on('end', async () => {
                  if (readData.length === 0) {
                    reject(null);
                  } else {
                    const stream = JSON.parse(readData).stream;

                    if (stream && stream.stream_type === 'live') {
                      resolve({
                        title: stream.channel.status,
                        url: url
                      })
                    } else {
                      reject(null);
                    }
                  }
                });
              });
              req.on('error', (err) => Utils.logError('Twitch', 'search', err.message));
              req.end();
            }
          }
        });
      });

      req.on('error', (err) => Utils.logError('Twitch', 'search', err.message));
      req.end();
    });
  }

  validateURL(url: string): boolean {
    return (url.indexOf("twitch.tv") !== -1);
  }

  getReadableStream(song: ISong): Promise<string> {
    return new Promise((resolve, reject) => {
      exec('streamlink --stream-url ' + song.url + ' audio_only', (error, stdout, stderr) => {
        if (error) reject(null);

        resolve(stdout.substring(stdout.indexOf('https'), stdout.indexOf('m3u8') + 4));
      });
    })
  }
}