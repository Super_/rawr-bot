import { Snowflake, VoiceConnection, Message, GuildMember, TextChannel, Channel, VoiceChannel } from "discord.js";

import { Utils } from "../../utils/Utils";
import { MusicConnection } from "../connection/MusicConnection";
import { MusicManager } from "../services/MusicManager";
import { MessageEmbed } from "../../commands/commands/embeds/MessageEmbed";
import { EmbedType } from "../../commands/commands/embeds/EmbedType";

export class BotController {
  private readonly _musicManager: MusicManager;
  private readonly _streamDispatcherVolumesMap: Map<Snowflake, number>;

  constructor(musicManager: MusicManager) {
    this._musicManager = musicManager;

    // TODO: Load from DB probably?
    this._streamDispatcherVolumesMap = new Map<Snowflake, number>();
  }

  /**
   * Starts the Bot playing music.
   * 
   * @param {Message} message the message that caused the bot to start playing.
   * @returns {Promise} resolves if the bot successfully connected to the voice channel, rejects otherwise.
   * @memberof BotController
   */
  async startPlaying(guildId: Snowflake, replyChannel: TextChannel, voiceChannel: VoiceChannel): Promise<void> {
    try {
      const musicConnection = await this.joinVoiceChannel(guildId, replyChannel, voiceChannel);
      musicConnection.start(this._musicManager, (guildId, replyChannel, reason) => this.leaveVoiceChannel(guildId, replyChannel, reason));
    } catch (err) {
      Utils.logError('BotController', 'startPlaying', err);
    }
  }

  /**
   * Stops the bot from playing.
   */
  stopPlaying(guildId: Snowflake, replyChannel: TextChannel, skipFlag: boolean = false, reason?: string): void {
    const musicConnection = this._musicManager.getCurrentConnection(guildId);
    if (musicConnection) {
      if (!skipFlag) {
        musicConnection.stop();
        this.leaveVoiceChannel(guildId, replyChannel, reason);
      } else {
        musicConnection.skip();
      }
    }
  }

  /**
   * Pauses the music.
   * 
   * @param {Message} message the message which paused the bot.
   * @returns {void}
   * @memberof BotController
   */
  pausePlaying(guildId: Snowflake): void {
    const musicConnection = this._musicManager.getCurrentConnection(guildId);

    if (musicConnection) musicConnection.pause();
  }

  /**
   * Resumes the music.
   *  
   * @param {Message} message the message which resumed the bot.
   * @returns {void}
   * @memberof BotController
   */
  resumePlaying(guildId: Snowflake): void {
    const musicConnection = this._musicManager.getCurrentConnection(guildId);

    if (musicConnection) musicConnection.resume();
  }

  setVolume(guildId: Snowflake, newVolume: number): void {
    this._streamDispatcherVolumesMap.set(guildId, newVolume);
    const musicConnection = this._musicManager.getCurrentConnection(guildId);
    if (musicConnection) {
      musicConnection.setVolume(newVolume);
    }
  }

  /**
   * Joins the voice channel for the given user who sent the message.
   */
  async joinVoiceChannel(guildId: Snowflake, replyChannel: TextChannel, voiceChannel: VoiceChannel): Promise<MusicConnection> {
    try {
      if (!this._streamDispatcherVolumesMap.has(guildId)) {
        this._streamDispatcherVolumesMap.set(guildId, 0.7);
      }

      const voiceConnection = await voiceChannel.join();
      const currentMusicConnection = this._musicManager.addConnection(guildId, {
        voiceConnection: voiceConnection,
        voiceChannel: voiceChannel,
        textChannel: replyChannel,
        streamDispatcherVolume: this._streamDispatcherVolumesMap.get(guildId)
      });

      return (currentMusicConnection);
    } catch (err) {
      Utils.logError('BotController', 'joinVoiceChannel', err);
    }
  }

  /**
   * Leaves a voice channel of a guild.
   */
  leaveVoiceChannel(guildId: Snowflake, replyChannel: TextChannel, reason?: string): void {
    const musicConnection = this._musicManager.getCurrentConnection(guildId);

    if (musicConnection && musicConnection.voiceChannel) {
      musicConnection.voiceChannel.leave();
      this._musicManager.destroyConnection(guildId);

      if (reason) {
        replyChannel.send(
          new MessageEmbed(
            EmbedType.INFORMATION,
            reason
          )
        )
      }
    }
  }
}