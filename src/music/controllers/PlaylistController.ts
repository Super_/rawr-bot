import { PlaylistDatabase } from "../connection/PlaylistDatabase";
import { Snowflake, TextChannel } from "discord.js";
import { Utils } from "../../utils/Utils";
import { MusicManager } from "../services/MusicManager";
import { IRequestedSong } from "../interfaces/IRequestedSong";
import { NowPlayingEmbed } from "../../commands/commands/embeds/NowPlayingEmbed";
import { AddedToPlaylistEmbed } from "../../commands/commands/embeds/AddedToPlaylistEmbed";
import { PlaylistEmbed } from "../../commands/commands/embeds/PlaylistEmbed";

export class PlaylistController {
  private readonly _musicManager: MusicManager;
  private readonly _playlistDatabase: PlaylistDatabase;

  /**
   * Creates an instance of PlaylistController.
   */
  constructor(musicManager: MusicManager, playlistDatabase: PlaylistDatabase) {
    this._musicManager = musicManager;
    this._playlistDatabase = playlistDatabase;
  }

  async getCurrentSong(guildId: Snowflake): Promise<object> {
    const nowPlayingEmbed = await new NowPlayingEmbed(this._musicManager.getCurrentConnection(guildId).currentSong).getEmbed();
    return nowPlayingEmbed;
  }

  /**
   * Adds a url to the guilds playlist.
   *
   * @param {Message} message the message for adding the song to the playlist.
   * @param {Object} song must contain elements "title" and "URL".
   * @returns {Promise<Boolean>} resolves true if we already have a connection, false otherwise.
   * @memberof MusicService
   */
  async addToPlaylist(guildId: Snowflake, replyChannel: TextChannel, playlistName: string, song: IRequestedSong): Promise<object> {
    try {
      await this._playlistDatabase.addToPlaylist(guildId, playlistName, song);

      const addedToPlaylistEmbed = await new AddedToPlaylistEmbed(song).getEmbed();
      return addedToPlaylistEmbed;
    } catch (err) {
      Utils.logError('PlaylistController', 'addToPlaylist', err);
    }
  }

  /**
   * Gets the playlist for this guild.
   */
  async getPlaylist(guildId: Snowflake, playlistName: string, replyChannel: TextChannel): Promise<object> {
    try {
      const result = await this._playlistDatabase.getPlaylist(guildId, playlistName);

      const playlistEmbed = await new PlaylistEmbed(result).getEmbed();
      return playlistEmbed;
    } catch (err) {
      Utils.logError('PlaylistController', 'getPlaylist', err);
    }
  }

  /**
   * Clears the playlist for this guild.
   *
   * @returns {Promise<any>} resolves if clearing the playlist was a success, reject otherwise.
   * @memberof MusicConnection
   */
  async clearPlaylist(guildId: Snowflake, playlistName: string): Promise<void> {
    try {
      await this._playlistDatabase.clearPlaylist(guildId, playlistName);
    } catch (err) {
      Utils.logError('PlaylistController', 'clearPlaylist', err);
    }
  }

  async removeFromPlaylist(guildId: Snowflake, playlistName: string, ids: number[]): Promise<object> {
    try {
      const currentPlaylist = await this._playlistDatabase.getPlaylist(guildId, playlistName);

      const toRemove = currentPlaylist.filter(obj => ids.indexOf(obj.resultIndex) !== -1).map(obj => obj.id);
      const newPlaylist = await Utils.getNormalisedSongListing(currentPlaylist.filter(obj => ids.indexOf(obj.resultIndex) === -1))
      await this._playlistDatabase.removeFromPlaylist(toRemove);

      const playlistEmbed = await new PlaylistEmbed(newPlaylist).getEmbed();
      return playlistEmbed;
    } catch (err) {
      Utils.logError('PlaylistController', 'removeFromPlaylist', err);
    }
  }
}