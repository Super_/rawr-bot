import { JSONInstanceLoader } from "../../instances/JSONInstanceLoader";
import { IMusicService } from "../interfaces/IMusicService";

const appRoot = require('app-root-path');
const config = require(appRoot + '/config/services.json');
/**
 * Loads music services from src/IO/music/config/services.json
 * 
 * @class MusicInstances
 * @extends {JSONInstanceLoader}
 */
 /* tslint:disable no-any */
export class MusicInstances extends JSONInstanceLoader {

  /**
   * Creates an instance of MusicInstances.
   * 
   * @param {Object} args the names and instances of classes available to pass as dependencies.
   * @memberof MusicInstances
   */
  constructor(args: any) {
    super(undefined, config, './music/services/', args);
  }

  /**
   * Creates a new instance of a file (req) with parameters taken from JSON and requires.
   * 
   * @param {String} req the class name to create an instance of.
   * @param {Object} json JSON object for parameters.
   * @param {Object[]} requires an object of all classes the new instance requires.
   * @returns {Object} a new instance of req.
   * @memberof CommandInstances
   */
  createInstance(req: any, json: { max_search_results: number }, requires: object[]) : IMusicService {
    return new req(
      json.max_search_results,
      ...requires
    );
  }

  get(instance: string): IMusicService {
    const inst = <IMusicService>this.instanceMap.get(instance);
    return inst !== null ? inst : null;
  }

  get instanceMap(): Map<string, IMusicService> {
    return <Map<string, IMusicService>>super.instanceMap;
  }
}
