import { ISong } from "./ISong";
export interface IOrderableSong extends ISong {
  resultIndex: number;
}