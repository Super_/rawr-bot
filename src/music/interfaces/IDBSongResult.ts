import { IRequestedSong } from "./IRequestedSong";

export interface IDBSongResult extends IRequestedSong {
  id?: number;
}