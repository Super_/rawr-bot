import { IOrderableSong } from "./IOrderableSongList";

export interface IPlaylist extends IOrderableSong {
  id: number;
}