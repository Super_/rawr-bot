import { IOrderableSong } from "./IOrderableSongList";
import { ISong } from "./ISong";
import { Readable } from "stream";

export interface IMusicService {
  search(message: string): Promise<{ searchString: string, results: IOrderableSong[] }>;
  getInformationFromURL(url: string): Promise<ISong>;
  validateURL(url: string): boolean;
  getReadableStream(song: ISong): Promise<string>;
}