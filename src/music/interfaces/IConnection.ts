import { VoiceConnection, VoiceChannel, TextChannel } from "discord.js";

export interface IConnection {
  voiceConnection: VoiceConnection;
  voiceChannel: VoiceChannel;
  textChannel: TextChannel;
  streamDispatcherVolume: number;
}