import { ISong } from "./ISong";
import { Snowflake } from "discord.js";

export interface IRequestedSong extends ISong {
  requestedby: Snowflake;
}