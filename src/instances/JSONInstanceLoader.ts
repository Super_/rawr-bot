import { WaitMessages } from "../commands/wait/WaitMessages";
import { Utils } from "../utils/Utils";
import { IMusicService } from "../music/interfaces/IMusicService";
import { ICommand } from "../commands/interfaces/ICommand";

/**
 * Loads JSON files and creates instances of classes based from what is listed in the JSON file.
 */
/* tslint:disable no-any */
export class JSONInstanceLoader {
  private readonly _waitMessages: WaitMessages;
  private _argumentMap: Map<string, object>;
  private _instanceMap: Map<string, IMusicService | ICommand>;

  /**
   * Creates an instance of JSONInstanceLoader.
   */
  constructor(waitMessages: WaitMessages, configJSON: any, baseDir: string, args: { [key: string]: object }) {
    this._waitMessages = waitMessages;

    this.setup(baseDir, configJSON, args);
  }

  private async setup(baseDir: string, configJSON: any, args: { [key: string]: object }): Promise<void> {
    try {
      this._argumentMap = await this._createArgumentMap(args);
      this._instanceMap = await this._createInstanceMap(baseDir, configJSON);
    } catch (err) {
      Utils.logError('JSONInstanceLoader', 'setup', err)
    }
  }

  /**
   * Gets an instance from a given name.
   */
  get(instance: string): object {
    const inst = this._instanceMap.get(instance);
    return inst !== null ? inst : null;
  }

  /**
   * Creates a new instance of a file (req) with parameters taken from JSON and requires.
   */
  createInstance(req: any, json: any, requires: object[]): IMusicService | ICommand {
    return new req(...requires);
  }

  /**
   * Creates a mapping of names to instances for all arguments passed into the constructor.
   * 
   * @param {Object} args names and instances to load 
   * @returns {Promise} resolve returns the argument map.
   * @memberof JSONInstanceLoader
   */
  private _createArgumentMap(args: { [key: string]: object }): Promise<Map<string, object>> {
    return new Promise(resolve => {
      const argsCount = Object.keys(args).length;

      if(argsCount === 0) {
        resolve(new Map<string, object>());
      } else {
        const tMap = new Map<string, object>();

        Object.keys(args).forEach(key => {
          tMap.set(key, args[key]);
  
          if (tMap.size === argsCount)
            resolve(tMap);
        });
      }
    });
  }

  /**
   * Creates a mapping of names to instances based from the information
   * listed in the given JSON file.
   */
  private _createInstanceMap(baseDir: string, configJSON: any): Promise<Map<string, IMusicService | ICommand>> {
    return new Promise((resolve
      , reject) => {
      const keyCount = Object.keys(configJSON).length;
      const tMap = new Map<string, IMusicService | ICommand>();

      Object.keys(configJSON).forEach(keyName => {
        const json = configJSON[keyName];
        const req = require('../' + baseDir + json.class);

        this._getArguments(json.requires).then(requires => {
          const reqKey = Object.keys(req).filter(
            value => value.toLowerCase() === (json.class.substr(2, json.class.lastIndexOf('.')-2)).toLowerCase()
          )[0];

          tMap.set(keyName, this.createInstance(req[reqKey], json, requires));

          if (tMap.size === keyCount)
            resolve(tMap);

        }).catch(err => reject(err));
      });
    });
  }

  /**
   * Looks at all the names given and returns a list of instances.
   * 
   * @param {String[]} requiredArgs a list of names of arguments needed. 
   * @returns {Promise} resolves a list of instances, rejects if any of them do not exist.
   * @memberof JSONInstanceLoader
   */
  private _getArguments(requiredArgs: string[]): Promise<object[]> {
    return new Promise((resolve, reject) => {
      if (requiredArgs.length === 0) {
        resolve([]);
      } else {
        const argsList: object[] = [];
        requiredArgs.forEach(name => {
          if (!this._argumentMap.has(name)) {
            reject('Unknown argument: ' + name);
          } else {
            argsList.push(this._argumentMap.get(name));

            if (argsList.length === requiredArgs.length)
              resolve(argsList);
          }
        });
      }
    });
  }

  get waitMessages(): WaitMessages {
    return this._waitMessages;
  }

  get instanceMap(): Map<string, IMusicService | ICommand> {
    return this._instanceMap;
  }
}