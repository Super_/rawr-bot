/* tslint:disable no-any */
export class DeferredPromise<T> {
  private _promise: Promise<T>;
  private _resolve: Function;
  private _reject: Function;

  constructor() {
    this._promise = new Promise((resolve, reject) => {
      this._resolve = resolve;
      this._reject = reject;
    });
  }

  resolve(value?: any): void {
    this._resolve(value);
  }

  reject(value?: any): void {
    this._reject(value);
  }

  get promise(): Promise<T> {
    return this._promise;
  }
}