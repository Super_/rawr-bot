import { Snowflake } from "discord.js";

import { WaitMessage } from "./WaitMessage";

/**
 * Handles each WaitMessage for a user.
 * 
 * @class WaitMessages
 */
export class WaitMessages {
  private readonly _waitMessages: Map<Snowflake, WaitMessage>;

  /**
   * Creates an instance of WaitMessages.
   */
  constructor() {
    this._waitMessages = new Map<Snowflake, WaitMessage>();
  }

  /**
   * Creates a WaitMessage for the given client.
   * 
   * @param {Snowflake} clientId the id for whom we're creating a wait message for.
   * @returns {WaitMessage} a new instance of WaitMessage.
   * @memberof WaitMessages
   */
  create(clientId: Snowflake, waitTime: number): WaitMessage {
    this._waitMessages.set(clientId, new WaitMessage(waitTime * 1000));
    return this._waitMessages.get(clientId);
  }

  /**
   * Completes a WaitMessage with the given content from the user.
   */
  resolve(clientId: Snowflake, content: string): void {
    this._waitMessages.get(clientId).accept(content);
    this.destroy(clientId);
  }

  /**
   * Destroys a wait message for a client. Removing it from the list of wait messages.
   */
  destroy(clientId: Snowflake): void {
    this._waitMessages.delete(clientId);
  }

  /**
   * Returns true if a command is waiting on input from this user, false otherwise.
   */
  isWaitingOn(clientId: Snowflake): boolean {
    return this._waitMessages.has(clientId);
  }
}