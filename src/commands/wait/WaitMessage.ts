import { DeferredPromise } from "./DeferredPromise";


/**
 * Stores the promise and timer for command waiting.
 */
export class WaitMessage {
  private readonly _timeout: number;
  private _timer: NodeJS.Timer;
  private _promise: DeferredPromise<string>;

  /**
   * Creates an instance of WaitMessage.
   */
  constructor(timeout: number) {
    this._timeout = timeout;
  }

  /**
   * Waits for the promise to be resolved or rejected.
   */
  wait(): Promise<string> {
    this._promise = new DeferredPromise();
    this._timer = setTimeout(() => this._promise.reject(), this._timeout);

    return this._promise.promise;
  }

  /**
   * Accepts a message from a user, ending the wait.
   */
  accept(message: string): void {
    clearTimeout(this._timer);
    this._promise.resolve(message);
  }
}