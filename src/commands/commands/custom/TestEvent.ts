import { Client, Message } from "discord.js";

import { Command } from "../Command";
import { WaitMessages } from "../../wait/WaitMessages";
import { MessageEmbed } from "../embeds/MessageEmbed";
import { RawrBot } from "../../../RawrBot";
import { EmbedType } from "../embeds/EmbedType";

export class TestEvent extends Command {

  constructor(waitMessages: WaitMessages, waitEnabled: boolean, waitTimeout: number, argsListing: object) {
    super(waitMessages, waitEnabled, waitTimeout, argsListing);
  }

  async process(message: Message, args: string[], client: Client): Promise<void> {
    const m_split = message.content.split(" ");
    if(m_split.length != 3) {
      return;
    }

    switch (m_split[2]) {
      case 'guildCreate':
        client.emit('guildCreate', message.guild);
        break;
      case 'guildDelete':
        client.emit('guildDelete', message.guild);
        break;
      default:
        return;
    }

    message.channel.send(
      await new MessageEmbed(EmbedType.SUCCESS, `Testing event: ` + m_split[2]).getEmbed()
    );
  }

}