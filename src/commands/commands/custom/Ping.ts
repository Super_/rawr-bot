import { Client, Message } from "discord.js";

import { Command } from "../Command";
import { WaitMessages } from "../../wait/WaitMessages";
import { MessageEmbed } from "../embeds/MessageEmbed";
import { RawrBot } from "../../../RawrBot";
import { EmbedType } from "../embeds/EmbedType";

export class Ping extends Command {

  constructor(waitMessages: WaitMessages, waitEnabled: boolean, waitTimeout: number, argsListing: object) {
    super(waitMessages, waitEnabled, waitTimeout, argsListing);
  }

  async process(message: Message, args: string[], client: Client): Promise<void> {
    message.channel.send(
      await new MessageEmbed(EmbedType.SUCCESS, `Pong! My ping is ${Math.round(client.ping)}ms!`).getEmbed()
    );
  }

}