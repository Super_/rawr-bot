import { Message } from "discord.js";

import { Command } from "../Command";
import { RawrBot } from "../../../RawrBot"
import { WaitMessages } from "../../wait/WaitMessages";
import { MessageEmbed } from "../embeds/MessageEmbed";
import { EmbedType } from "../embeds/EmbedType";

const config = require('../../../../config.json');

export class Version extends Command {

  constructor(waitMessages: WaitMessages, waitEnabled: boolean, waitTimeout: number, argsListing: object) {
    super(waitMessages, waitEnabled, waitTimeout, argsListing);
  }

  async process(message: Message): Promise<void> {
    await message.channel.send(
      await new MessageEmbed(EmbedType.SUCCESS, `I'm currently on version ${RawrBot.VERSION}!`).getEmbed()
    );
  }

}