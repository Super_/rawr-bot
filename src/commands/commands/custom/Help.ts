import { Command } from "../Command";
import { WaitMessages } from "../../wait/WaitMessages";
import { Message } from "discord.js";
import { Utils } from "../../../utils/Utils";

const appRoot = require('app-root-path');
const commandConfig = require(appRoot + '/config/commands.json');

export class Help extends Command {

  constructor(waitMessages: WaitMessages, waitEnabled: boolean, waitTimeout: number, argsListing: object) {
    super(waitMessages, waitEnabled, waitTimeout, argsListing);
  }

  async process(message: Message, args: string[]): Promise<void> {
    const superReturn = await super.process(message, args);
    let commandArg = null;

    // TODO: super.process maybe should do callbacks depending on the result
    // instead of returning a string..
    if (superReturn === 'user_input_cancelled') {
      return;
    } else if (superReturn === 'got_all_args') {
      commandArg = args[0];
    } else {
      commandArg = (<string>superReturn);
    }

    if (commandArg.toLowerCase() === 'music') {       
      const embed = await Utils.generateMusicHelpEmbed();

      message.channel.send({embed});
    } else {
      message.reply('I only made a help message for music because I\'m lazy and wanted to sleep');
    }
  }
}