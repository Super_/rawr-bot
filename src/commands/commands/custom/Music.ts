import { Message, TextChannel, Snowflake, User, VoiceChannel } from "discord.js";

import { Command } from "../Command";
import { Utils } from "../../../utils/Utils";
import { MusicManager } from "../../../music/services/MusicManager";
import { PlaylistController } from "../../../music/controllers/PlaylistController";
import { BotController } from "../../../music/controllers/BotController";
import { WaitMessages } from "../../wait/WaitMessages";
import { IRequestedSong } from "../../../music/interfaces/IRequestedSong";
import { IOrderableSong } from "../../../music/interfaces/IOrderableSongList";
import { SearchEmbed } from "../embeds/SearchEmbed";
import { MessageEmbed } from "../embeds/MessageEmbed";
import { EmbedType } from "../embeds/EmbedType";

export class Music extends Command {
  private readonly _musicManager: MusicManager;
  private readonly _playlistController: PlaylistController;
  private readonly _botController: BotController;

  constructor(waitMessages: WaitMessages, waitEnabled: boolean, waitTimeout: number, argsListing: object, musicManager: MusicManager) {
    super(waitMessages, waitEnabled, waitTimeout, argsListing);

    this._musicManager = musicManager;

    this._playlistController = musicManager.playlistController;
    this._botController = musicManager.botController;
  }

  async process(message: Message, args: string[]): Promise<void> {

    const findCommand = async () => {
      switch (args[0]) {
        case 'play':
        case 'p':
          return this.play(message, args);

        case 'stop':
          return this.stop(message, args);
        case 'skip':
          return this.stop(message, args, true);

        case 'pause':
          return this.pause(message, args);
        case 'resume':
          return this.resume(message, args);

        case 'queue':
        case 'q':
          return await this.queue(message, args);

        case 'playing':
          return await this.playing(message, args);

        case 'volume':
        case 'v':
          return await this.volume(message, args);

        default:
          return false;
      }
    }

    if (!await findCommand()) {
      this.search(message, args);
    }
  }

  play(message: Message, args: string[]): boolean {
    if (!this.isInVoice(message)) return true;
    if (args.length !== 1 && args.length !== 2) return false;

    if (args.length === 1) {
      this._botController.startPlaying(message.guild.id, <TextChannel>message.channel, message.member.voiceChannel);
    } else {
      this.playFromURL(args[1], message.guild.id, <TextChannel>message.channel, message.author.id, message.member.voiceChannel);
    }
    return true;
  }

  stop(message: Message, args: string[], skipFlag: boolean = false): boolean {
    if (!this.isInVoice(message)) return true;
    if (args.length !== 1) return false;

    this._botController.stopPlaying(message.guild.id, <TextChannel>message.channel, skipFlag);
    return true;
  }

  // TODO: make embed with song name for wooosh and progress bar maybe? ooo use my fav icons
  pause(message: Message, args: string[]): boolean {
    if (!this.isInVoice(message)) return true;
    if (args.length !== 1) return false;

    this._botController.pausePlaying(message.guild.id);
    return true;
  }

  // TODO: make embed with song name for wooosh and progress bar maybe? ooo use my fav icons
  resume(message: Message, args: string[]): boolean {
    if (!this.isInVoice(message)) return true;
    if (args.length !== 1) return false;

    this._botController.resumePlaying(message.guild.id);
    return true;
  }

  async queue(message: Message, args: string[]): Promise<boolean> {
    if (args.length > 1 && !this.isInVoice(message)) return true;

    try {
      if (args.length === 1) /* view queue */ {
        message.channel.send(
          await this._playlistController.getPlaylist(message.guild.id, 'default', <TextChannel>message.channel)
        );
        return true;
      } else if (args.length === 2) {
        if (args[1] === 'clear' || args[1] === 'c') /* clear queue */ {
          await this._playlistController.clearPlaylist(message.guild.id, 'default');
          message.channel.send(
            new MessageEmbed(EmbedType.SUCCESS, 'Playlist cleared!')
          )
        } else /* add to queue */ {
          this.playFromURL(args[1], message.guild.id, <TextChannel>message.channel, message.author.id, message.member.voiceChannel);
        }
        return true;
      } else {
        if (args[1] === 'remove' || args[1] === 'r') /* remove from queue */ {
          const songIdsNumber = args.splice(2).map(value => Number.parseInt(value));
          const allNumbers = songIdsNumber.every(value => value !== Number.NaN);
          if (allNumbers) {
            message.channel.send(
              await this._playlistController.removeFromPlaylist(message.guild.id, 'default', songIdsNumber)
            );
          } else {
            message.channel.send(
              new MessageEmbed(EmbedType.ERROR, 'Make sure all the ids are numbers.')
            )
          }
          return true;
        }
      }
    } catch (err) {
      message.channel.send(
        new MessageEmbed(EmbedType.ERROR, 'Something seems to have gone wrong: ' + err)
      )
      Utils.logError('Music', 'queue', err, args);
      return true;
    }
    return false;
  }

  async playing(message: Message, args: string[]): Promise<boolean> {
    if (args.length > 1) return false;

    try {
      if (!this._musicManager.isPlaying(message.guild.id)) {
        message.channel.send(
          new MessageEmbed(EmbedType.INFORMATION, 'I am not playing anything!')
        )
        return true;
      }

      message.channel.send(
        await this._playlistController.getCurrentSong(message.guild.id)
      );

      return true;
    } catch (err) {
      message.channel.send(
        new MessageEmbed(EmbedType.ERROR, 'Something seems to have gone wrong: ' + err)
      )
      Utils.logError('Music', 'playing', err, args);
      return true;
    }
  }

  volume(message: Message, args: string[]): boolean {
    if (!this.isInVoice(message)) return true;
    if (args.length > 2) return false;

    if (args.length === 1) {
      if (this._musicManager.isPlaying(message.guild.id)) {
        const vol = this._musicManager.getCurrentConnection(message.guild.id).currentVolume;

        message.channel.send(
          new MessageEmbed(
            EmbedType.SUCCESS,
            'The current volume is ' + vol
          )
        )
      } else {
        // TODO: Read volume from database etc.
        message.channel.send(
          new MessageEmbed(
            EmbedType.INFORMATION,
            'I am not currently playing.'
          )
        )
      }
    } else {
      let newVolume = Number.parseFloat(args[1]);
      if (Number.isNaN(newVolume)) {
        message.channel.send(
          new MessageEmbed(EmbedType.INFORMATION, 'https://www.youtube.com/watch?v=YKcabMXAjUU')
        )
      } else {
        let replyMessage = '';
        if (newVolume > 1.0) {
          newVolume = 1.0;
          replyMessage = 'Due to ~~console~~ API limitations, volume is capped at 1. \n\n'
        } else if (newVolume < 0.0) {
          newVolume = 0.0;
          replyMessage = 'I won\'t even ask why you\'re trying to set the volume lower than 0. \n\n'
        }
        this._botController.setVolume(message.guild.id, newVolume);

        message.channel.send(
          new MessageEmbed(
            EmbedType.SUCCESS,
            replyMessage + 'The new volume is: ' + newVolume
          )
        )
      }
    }
    return true;
  }

  async search(message: Message, args: string[]): Promise<void> {
    if (!this.isInVoice(message)) return;

    try {
      const searchResult = await this._musicManager.getService('YouTube').search(args.join(' '));
      const optionOkay = await this._processSearchReply(message.author.id, message.guild.id, <TextChannel>message.channel, searchResult.searchString, searchResult.results);
      if (optionOkay) {
        if (!this._musicManager.isPlaying(message.guild.id))
          this._botController.startPlaying(message.guild.id, <TextChannel>message.channel, message.member.voiceChannel);
      }
    } catch (err) {
      message.channel.send(
        new MessageEmbed(EmbedType.ERROR, 'Something seems to have gone wrong: ' + err)
      )
      Utils.logError('Music', 'search', err, args);
    }
  }


  isInVoice(message: Message): boolean {
    const vc = message.member.voiceChannel;
    const botVC = this._musicManager.getBotVoiceChannel(message.guild.id);

    if (!vc || (botVC && vc.id !== botVC.id)) {
      message.channel.send(
        new MessageEmbed(
          EmbedType.INFORMATION, 
          'you must be in a voice channel, or the same voice channel as me if I am already in one, to do this!', 
          message.author.id)
      )
      return false;
    } else {
      return true;
    }
  }

  async playFromURL(url: string, guildId: Snowflake, channel: TextChannel, requestedby: Snowflake, voiceChannel: VoiceChannel): Promise<void> {
    try {
      const service = this._musicManager.getServiceFromURL(url);
      if (!service) {
        channel.send(
          new MessageEmbed(
            EmbedType.INFORMATION,
            'That url does not look like something I can play! :thinking:'
          )
        )
        return;
      }
      const videoInfo = await service.getInformationFromURL(url);
      channel.send(
        await this._playlistController.addToPlaylist(
          guildId,
          channel,
          'default',
          { title: videoInfo.title, url: videoInfo.url, requestedby }
        )
      );

      if (!this._musicManager.isPlaying(guildId))
        this._botController.startPlaying(guildId, channel, voiceChannel);
    } catch (err) {
      channel.send(
        new MessageEmbed(EmbedType.ERROR, 'Something seems to have gone wrong: ' + err)
      )
      Utils.logError('Music', 'playFromURL', err, [url]);
    }
  }

  async _processSearchReply(authorId: Snowflake, guildId: Snowflake, replyChannel: TextChannel, originalSearch: string, resultArray: IOrderableSong[]): Promise<boolean> {
    try {
      const searchEmbed = await new SearchEmbed(originalSearch, resultArray, this.waitTimeout).getEmbed();
      await replyChannel.send(searchEmbed);

      const selection = await this.getUserInput(authorId, replyChannel, this.waitTimeout);
      if (selection === '') {
        replyChannel.send(
          new MessageEmbed(EmbedType.INFORMATION, 'cancelled.', authorId)
        )
        return false;
      } else {
        if (selection === 'cancel') {
          replyChannel.send(
            new MessageEmbed(EmbedType.SUCCESS, 'okay! Cancelled.', authorId)
          )
          return false;
        } else {
          const resultIndex = resultArray.findIndex(val => val.resultIndex === Number.parseInt(selection));

          if (resultIndex !== -1) {
            const songObj = resultArray[resultIndex];

            replyChannel.send(
              await this._playlistController.addToPlaylist(
                guildId, 
                replyChannel, 
                'default', 
                { title: songObj.title, url: songObj.url, requestedby: authorId }
              )
            );
            return true;
          } else {
            replyChannel.send(
              new MessageEmbed(EmbedType.INFORMATION, 'that was an unknown selection.', authorId)
            )
            return false;
          }
        }
      }
    } catch (err) {
      replyChannel.send(
        new MessageEmbed(EmbedType.ERROR, 'Something seems to have gone wrong: ' + err)
      )
      return false;
    }
  }

}