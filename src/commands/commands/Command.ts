import { Message, Client, TextChannel } from "discord.js";

import { WaitCommand } from "./WaitCommand";
import { WaitMessages } from "../wait/WaitMessages";

export abstract class Command extends WaitCommand {
  private readonly _waitEnabled: boolean;
  private readonly _waitTimeout: number;
  private readonly _argsListing: string[];

  constructor(waitMessages: WaitMessages, waitEnabled: boolean, waitTimeout: number, args: object) {
    super(waitMessages);

    this._waitEnabled = waitEnabled;
    this._waitTimeout = waitTimeout;
    this._argsListing = Object.keys(args);
  }

  /**
   * Async. Processes a given command message and its arguments.
   * 
   * @param {Message} message the command message.
   * @param {String[]} args the arguments of the command.
   * @returns {String|Boolean} String if the command was waiting for a parameter and it was given, 
   * false if the command was handled in this process method, true if the command needs to be
   * handled in the main command process method.
   * @memberof Command
   */
  async process(message: Message, args: string[], client?: Client) : Promise<string | void> {
    if (this._waitEnabled) {
      /* Limiting waiting to single argument commands for now. */
      if ((args.length < this.argsListingLength) && this.argsListingLength === 1) {
        const reason = 'You didn\'t enter the value for **' + this._argsListing[0] + '**!\n\n' +
          'Please enter it as your next message (timeout is ' + this._waitTimeout + ' seconds).';

        const ret = await this.getUserInput(message.author.id, <TextChannel>message.channel, this.waitTimeout, reason);
        if (ret === 'cancelled') return 'user_input_cancelled';

        return ret;
      }
    }
    return 'got_all_args';
  }

  get waitEnabled(): boolean {
    return this._waitEnabled;
  }

  get waitTimeout(): number {
    return this._waitTimeout;
  }

  get argsListing(): string[] {
    return this._argsListing;
  }

  get argsListingLength(): number {
    return this.argsListing.length;
  }
}