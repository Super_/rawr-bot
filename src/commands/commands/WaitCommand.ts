import { Message, Snowflake, TextChannel, Client } from "discord.js";

import { WaitMessages } from "../wait/WaitMessages";

import { ICommand } from "../interfaces/ICommand";

/**
 * Provides functionality for all commands to use the "Waiting" feature.
 * 
 * @class WaitCommand
 */
export abstract class WaitCommand implements ICommand {
  private readonly _waitMessages: WaitMessages;

  constructor(waitMessages: WaitMessages) {
    this._waitMessages = waitMessages;
  }

  private _wait(userId: Snowflake, replyChannel: TextChannel, waitTime: number, reason?: string): Promise<string> {
    // TODO: Maybe be more informative? Need to think of a good way to do this.
    // TODO: This is good enough for now.
    if (reason) replyChannel.send(reason);

    return new Promise((resolve, reject) => {
      const msg = this._waitMessages.create(userId, waitTime);

      msg.wait().then(msg => resolve(msg)).catch(() => {
        this._waitMessages.destroy(userId);
        reject();
      });
    });
  }

  /**
   * Waits for the missing parameter to be given.
   * 
   * TODO: Extend this to allow checking of the required input type.
   */
  public getUserInput(userId: Snowflake, replyChannel: TextChannel, waitTime: number, reason?: string): Promise<string> {
    return new Promise((resolve) => {
      this._wait(userId, replyChannel, waitTime, reason).then(m => resolve(m)).catch(() => resolve(''));
    });
  }
  
  abstract process(message: Message, args: string[], client?:Client): Promise<string | void>;
}