import { IRequestedSong } from "../../../music/interfaces/IRequestedSong";
import { Embed } from "./Embed";
import { EmbedType } from "./EmbedType";

import { RichEmbed, Snowflake } from "discord.js";

export class MessageEmbed extends Embed {

  constructor(type: EmbedType, information: string, replyToUser?: Snowflake) {
    super(type, {
      description: replyToUser ? `<@${replyToUser}>, ` + information : information
    });
  }
  
  protected async generateEmbed(): Promise<RichEmbed> {
    return this.embed;
  }
}