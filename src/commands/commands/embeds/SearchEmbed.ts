import { IOrderableSong } from "../../../music/interfaces/IOrderableSongList";
import { Utils } from "../../../utils/Utils";
import { Embed } from "./Embed";
import { RichEmbed } from "discord.js";
import { EmbedType } from "./EmbedType";

export class SearchEmbed extends Embed {
  private readonly _resultArray: IOrderableSong[];

  constructor(searchString: string, resultArray: IOrderableSong[], timeout: number) {
    super(EmbedType.SUCCESS, {
      title: `Search results for ${searchString}`,
      footer: {
        text: `Reply with a number or reply 'cancel' to cancel. Automatically cancels after ${timeout} seconds.`
      }
    });

    this._resultArray = resultArray;
  }

  protected async generateEmbed(): Promise<RichEmbed> {
    const orderedList = await Utils.getNormalisedSongListing(this._resultArray);
    let fieldString = ``;

    for (let i = 0; i < orderedList.length; i++) {
      fieldString += `[${this._resultArray[i].resultIndex}] ${this._resultArray[i].title}\n`;

      if (i == this._resultArray.length - 1)
        this.embed.addField('\u200b', fieldString);
    }

    return this.embed;
  }
}