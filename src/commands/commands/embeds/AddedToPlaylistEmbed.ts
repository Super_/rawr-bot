import { Embed } from "./Embed";
import { ISong } from "../../../music/interfaces/ISong";
import { RichEmbed } from "discord.js";
import { EmbedType } from "./EmbedType";

export class AddedToPlaylistEmbed extends Embed {
  private readonly _song: ISong;

  constructor(song: ISong) {
    super(EmbedType.SUCCESS, {
      title: song.title,
      url: song.url,
      author: {
        name: 'Added to Playlist:'
      },
      fields: [{
        name: '\u200b',
        value: 'Type **rawr music queue** to see the queue.'
      }]
    });

    this._song = song;
  }
  
  protected async generateEmbed(): Promise<RichEmbed> {
    return this.embed;
  }
}