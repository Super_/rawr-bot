import { RichEmbed, RichEmbedOptions } from "discord.js";
import { IEmbed } from "../../interfaces/IEmbed";
import { EmbedType } from "./EmbedType";

export abstract class Embed implements IEmbed {
  private readonly _embed: RichEmbed;

  constructor(type: EmbedType, data: RichEmbedOptions) {
    this._embed = new RichEmbed({
      color: type,
      ...data
    });
  }

  protected abstract async generateEmbed() : Promise<RichEmbed>;

  protected get embed() : RichEmbed {
    return this._embed;
  }

  async getEmbed() : Promise<RichEmbed> {
    return await this.generateEmbed();
  }
}