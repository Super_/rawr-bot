import { Embed } from "./Embed";
import { IOrderableSong } from "../../../music/interfaces/IOrderableSongList";
import { RichEmbed } from "discord.js";
import { EmbedType } from "./EmbedType";

export class PlaylistEmbed extends Embed {
  private readonly _resultArray: IOrderableSong[];

  constructor(resultArray: IOrderableSong[]) {
    super(EmbedType.SUCCESS, {
      title: 'Queue:'
    });

    this._resultArray = resultArray;
  }
  
  protected async generateEmbed(): Promise<RichEmbed> {
    if (this._resultArray.length === 0) {
      this.embed.addField('\u200b', 'Nothing here!');
    } else {
      let fieldString = ``;

      for (let i = 0; i < this._resultArray.length; i++) {
        fieldString += `${this._resultArray[i].resultIndex}. \t${this._resultArray[i].title}\n`;

        if (i == this._resultArray.length - 1) 
          this.embed.addField('\u200b', fieldString);
      }
    }

    return this.embed;
  }
}