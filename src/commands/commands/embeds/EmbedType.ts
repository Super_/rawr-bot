export enum EmbedType {
  SUCCESS = 0x00FF00,
  INFORMATION = 0xFF5600,
  ERROR = 0xFF0000
}