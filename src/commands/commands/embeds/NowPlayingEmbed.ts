import { Embed } from "./Embed";
import { IRequestedSong } from "../../../music/interfaces/IRequestedSong";
import { RichEmbed, Snowflake } from "discord.js";
import { EmbedType } from "./EmbedType";

export class NowPlayingEmbed extends Embed {
  private readonly _requestedSong: IRequestedSong;

  constructor(requestedSong: IRequestedSong) {
    super(EmbedType.SUCCESS, {
      title: requestedSong.title,
      url: requestedSong.url,
      author: {
        name: 'Now Playing:'
      },
      fields: [{
        name: '\u200b',
        value: `Requested by <@${requestedSong.requestedby}>`
      }]
    });

    this._requestedSong = requestedSong;
  }

  protected async generateEmbed(): Promise<RichEmbed> {
    return this.embed;
  }
}