import { RichEmbed } from "discord.js";

export interface IEmbed {
  getEmbed() : Promise<RichEmbed>;
}