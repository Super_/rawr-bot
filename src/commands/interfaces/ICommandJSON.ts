export interface ICommandJSON {
  class: string;
  requires: string[];
  args: [{ name: string, type: {} }];
  wait_enabled: boolean;
  wait_timeout?: number;
}