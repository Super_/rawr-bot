import { Message, Client } from "discord.js";

export interface ICommand {
  process(message: Message, args: string[], client?:Client): Promise<string | void>
}