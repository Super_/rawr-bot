
import { JSONInstanceLoader } from "../../instances/JSONInstanceLoader";
import { WaitMessages } from "../wait/WaitMessages";
import { ICommand } from "../interfaces/ICommand";
import { ICommandJSON } from "../interfaces/ICommandJSON";

const appRoot = require('app-root-path');
const config = require(appRoot + '/config/commands.json');

/**
 * Loads commands from src/IO/commands/config/commands.json.
 * 
 * @extends {JSONInstanceLoader}
 */

 /* tslint:disable no-any */
export class CommandInstances extends JSONInstanceLoader {

  constructor(waitMessages: WaitMessages, args: any) {
    super(waitMessages, config, './commands/commands/custom/', args);
  }

  /**
   * Creates a new instance of a file (req) with parameters taken from JSON and requires.
   */
  createInstance(req: any, json: ICommandJSON, requires: object[]): ICommand {
    return new req(
      this.waitMessages,
      json.wait_enabled,
      json.wait_timeout,
      json.args,
      ...requires
    );
  }

  get(instance: string): ICommand {
    const inst = <ICommand>(this.instanceMap.get(instance));
    return inst !== null ? inst : null;
  }

  get instanceMap(): Map<string, ICommand> {
    return <Map<string, ICommand>>super.instanceMap;
  }
}