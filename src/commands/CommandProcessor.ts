import {Message, Client} from "discord.js";
import {MusicManager} from "../music/services/MusicManager";
import {WaitMessages} from "./wait/WaitMessages";
import {CommandInstances} from "./instances/CommandInstances";

/**
 * Processes messages from users to determine what actions to take.s
 * 
 * @class CommandProcessor
 */
export class CommandProcessor {
  private readonly _prefix: string;
  private readonly _musicManager: MusicManager;
  private readonly _waitMessages: WaitMessages;
  private readonly _instances: CommandInstances;

  constructor(prefix: string, musicManager: MusicManager) {
    this._prefix = prefix;

    this._waitMessages = new WaitMessages();
    this._musicManager = musicManager;

    this._instances = new CommandInstances(
      this._waitMessages, {
        'MusicManager': musicManager
      }
    );
  }

  /**
   * Processes messages to check what command to run.
   * 
   * If we're waiting on a message then bypass command running and
   * resolve the wait.
   * 
   * @param {Message} message the message sent.
   * @param {Client} client the discord client.
   * @returns {void} nothing.
   * @memberof CommandProcessor
   */
  process(message: Message, client: Client): void {
    if (message.author.bot) return;

    if (this._waitMessages.isWaitingOn(message.author.id)) {
      let commandString = message.content;
      if (message.content.startsWith(this._prefix)) {
        const args = message.content.slice(this._prefix.length).trim().split(/ +/g);
        commandString = args.shift().toLowerCase();
      }

      this._waitMessages.resolve(message.author.id, commandString);
      return;
    }

    if (!message.content.toLowerCase().startsWith(this._prefix)) return;

    const args = message.content.slice(this._prefix.length).trim().split(/ +/g);
    const commandString = args.shift().toLowerCase();
    const command = this._instances.get(commandString);
    if (command === undefined) return;

    command.process(message, args, client);
  }

}