import { Snowflake, TextChannel, Client } from "discord.js";
import { IOrderableSong } from "../music/interfaces/IOrderableSongList";
import { IRequestedSong } from "../music/interfaces/IRequestedSong";

const fs = require('fs');

/**
 * Has static utility functions.
 * 
 * @class Utils
 */
export class Utils {

  /**
   * Converts milliseconds to minutes and seconds for pretty printing.
   * 
   * @static
   * @param {any} milliseconds the Number of milliseconds to convert.
   * @returns {String} format XX:XX
   * @memberof Utils
   */
  static millisecondsToMinutesAndSeconds(milliseconds: number): string {
    const minutes: number = Math.floor(milliseconds / 60000);
    const seconds: string = ((milliseconds % 60000) / 1000).toFixed(0);

    return (seconds === '60' ? (minutes + 1) + ':00' : minutes + ':' + (seconds < '10' ? '0' : '') + seconds);
  }

  // leaving this here because its shit and I'm getting rid of it soon.
  static generateMusicHelpEmbed(): Promise<object> {
    return new Promise(resolve => {
      resolve({
        color: 14277325,
        footer: {
          text: "Everything in [] is optional. Everything in <> is mandatory. \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b  Everything with a ^ you must be in the same voice channel as the bot. \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b \u200b Do not include [], <> or ^ in the command.  \u200b \u200b \u200b \u200b \u200b \u200b  \u200b \u200b \u200b \u200b \u200b \u200b  \u200b \u200b \u200b \u200b \u200b \u200b  \u200b \u200b \u200b \u200b \u200b \u200b  \u200b \u200b \u200b \u200b \u200b \u200b  \u200b \u200b \u200b \u200b \u200b \u200b  \u200b \u200b \u200b \u200b \u200b \u200b  \u200b \u200b \u200b \u200b \u200b \u200b  \u200b \u200b \u200b \u200b \u200b \u200b  \u200b \u200b \u200b \u200b \u200b \u200b  \u200b \u200b \u200b \u200b \u200b \u200b  \u200b \u200b \u200b \u200b \u200b \u200b  \u200b \u200b \u200b \u200b \u200b \u200b  \u200b \u200b \u200b \u200b \u200b \u200b  \u200b \u200b \u200b \u200b \u200b \u200b  \u200b \u200b \u200b \u200b \u200b \u200b  \u200b \u200b \u200b \u200b \u200b \u200b Only the bold parts of the command name are required (For example, p is short for play)."
        },
        author: {
          name: "Music"
        },
        title: "rawr music *command*",
        fields: [{
          name: "\u200b",
          value:
            "**p**lay^** [ url ]**\n**playing**\n**stop^**\n**skip^**\n**pause^**\n**r**esume^\n**q**ueue **[** **c**lear **]** / **[** **r**emove^ **id(s) ] **/** [ url ]**\n**v**olume^ **<value between 0.0 and 1.0>**"
        }]
      });
    });
  }

  /**
   * Logs errors to a file.
   * 
   * @static
   * @param {any} cls the name of the class.
   * @param {any} func the name of the function.
   * @param {any} message the error message.
   * @returns {void}
   * @memberof Utils
   */
  static logError(cls: string, func: string, message: string, params?: string[]): void {
    fs.appendFile('./logs/err.log', `[${cls}->${func} @ ${new Date(Date.now()).toLocaleString()}]: ${message}\r\n`, (err: string) => {
      if (err) {
        console.log(`Error writing an error to a file: ${err}`);
        console.log(`[${cls}->${func} @ ${new Date(Date.now()).toLocaleString()}]: ${message}\r\n`);
      }
    });
  }

  static replyViaChannel(authorId: Snowflake, replyChannel: TextChannel, message: string): void {
    replyChannel.send(`<@${authorId}>, ${message}`);
  }

  static getNormalisedSongListing(result: IOrderableSong[]): Promise<IOrderableSong[]> {
    return new Promise((resolve) => {
      for (let i = 0; i < result.length; i++) {
        result[i].resultIndex = i + 1;

        if (i == result.length - 1) resolve(result);
      }
    });
  }
}