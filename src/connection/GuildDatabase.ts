import { Client } from "pg";
import { Utils } from "../utils/Utils";
import { Snowflake } from "discord.js";

export class GuildDatabase {
    private _connection: Client;

    constructor(connection: Client) {
        this._connection = connection;
    }

    addGuild(guildId: Snowflake): void {
        this._connection.query(
            'INSERT INTO guild (id) VALUES ($1)', [guildId],
            (error, result) => {
                if (error) Utils.logError('GuildDatabase', 'addGuild', error.message);
            }
        );
    }

    deleteGuild(guildId: Snowflake): void {
        this._connection.query(
            'DELETE FROM guild WHERE id = $1', [guildId],
            (error, results) => {
                if (error) Utils.logError('GuildDatabase', 'deleteGuild', error.message);
            }
        );
    }
}