import * as pg from "pg";

const appRoot = require('app-root-path');

/**
 * Controls the main database connection and collections.
 *
 * @class MySQLConnection
 */

export class DBConnection {
  private _connection: pg.Client;

  /**
   * Creates an instance of MySQLConnection.
   * @memberof DBConnection
   */
  constructor(db_user: string, db_pass:string, db_name: string) {
    this._connection = new pg.Client(`postgres://${db_user}:${db_pass}@localhost:5432/${db_name}`);
  }

  /**
   *
   *
   * @memberof DBConnection
   */
  connect(): Promise<void> {
    return this._connection.connect();
  }

  /**
   *
   * @type {Connection}
   * @readonly
   * @memberof DBConnection
   */
  get connection(): pg.Client {
    return this._connection;
  }
}